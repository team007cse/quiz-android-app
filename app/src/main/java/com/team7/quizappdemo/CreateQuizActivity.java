package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CreateQuizActivity extends Activity {
    JSONParser_old jsonParser = new JSONParser_old();

    private static String url_of_sending_question = "http://mohamedessam.juplo.com/quiz_app/postQuestionIntheQuiz.php";
    public String quiz_id ;
    public String instructor_id ;
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);

        quiz_id = getIntent().getExtras().getString("quiz_id");
        instructor_id = getIntent().getExtras().getString("instructor_id");
//Edits
        EditText questionHeader = (EditText)findViewById(R.id.question_header);
        EditText choose_a = (EditText) findViewById(R.id.choose_a);
        EditText choose_b = (EditText) findViewById(R.id.choose_b);
        EditText choose_c = (EditText) findViewById(R.id.choose_c);
        EditText choose_d = (EditText) findViewById(R.id.choose_d);
        EditText answer = (EditText) findViewById(R.id.answer);

        //Buttons
        Button next = (Button) findViewById(R.id.next_question);
        Button finish = (Button) findViewById(R.id.finish);
        //next Question handler

        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

               new CreateOneQuestion().execute();
                Intent noExit = new Intent(getApplicationContext(), CreateQuizActivity.class);
                startActivity(noExit);
                finish();
            }
        });
        finish.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new CreateOneQuestion().execute();
                Intent exit = new Intent(getApplicationContext(), ChooseGroup.class);
                exit.putExtra("quiz_id",quiz_id);
                exit.putExtra("instructor_id",instructor_id);
                startActivity(exit);
                finish();
            }
        });
    }

    class CreateOneQuestion extends AsyncTask<String , String , String>{
        //Edits
        EditText questionHeader = (EditText)findViewById(R.id.question_header);
        EditText choose_a = (EditText) findViewById(R.id.choose_a);
        EditText choose_b = (EditText) findViewById(R.id.choose_b);
        EditText choose_c = (EditText) findViewById(R.id.choose_c);
        EditText choose_d = (EditText) findViewById(R.id.choose_d);
        EditText answer = (EditText) findViewById(R.id.answer);

        @Override
        protected String doInBackground(String... arg) {
            //for (int i = 0 ; i < 5 ; i++)
            //{
                String questionHeaderStr = questionHeader.getText().toString();
                String choose_aStr = choose_a.getText().toString();
                String choose_bStr = choose_b.getText().toString();
                String choose_cStr = choose_c.getText().toString();
                String choose_dStr = choose_d.getText().toString();
                String answerStr = answer.getText().toString();
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("question_header", questionHeaderStr));
            params.add(new BasicNameValuePair("choose_a", choose_aStr));
            params.add(new BasicNameValuePair("choose_b", choose_bStr));
            params.add(new BasicNameValuePair("choose_c", choose_cStr));
            params.add(new BasicNameValuePair("choose_d", choose_dStr));
            params.add(new BasicNameValuePair("answer", answerStr));
            params.add(new BasicNameValuePair("quiz_id", quiz_id));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_of_sending_question,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product
                    //Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                    //startActivity(i);

                    // closing this screen
                    //finish();
                } else {
                    // failed to create product

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            //}
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //ProgressDialog pDialog = new ProgressDialog(CreateQuizActivity.this);
            //pDialog.setMessage("Creating Question..");
            //pDialog.setIndeterminate(false);
            //pDialog.setCancelable(true);
            //pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
