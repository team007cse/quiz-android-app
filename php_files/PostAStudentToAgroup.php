<?php


// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['group_id']) && isset($_POST['student_id']) ) {

    $group_id = $_POST['group_id'];
    $student_id = $_POST['student_id'];

    // include db connect class
    require_once 'db.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysql_query("UPDATE students SET group_id = '$group_id' WHERE id = '$student_id'");

    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "student successfully sent.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";

        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>