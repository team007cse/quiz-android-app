package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;




import android.content.Intent;
import android.os.Environment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.TextView;

import  com.team7.quizappdemo.library.DatabaseHandler;
import com.team7.quizappdemo.library.UserFunctions;


import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import java.io.File;
import java.util.List;


public class ChooseQuiz extends Activity implements AdapterView.OnItemSelectedListener {
    Button bt;
    Button btLogout;
    Spinner spinner;
    //String [] quizzesfromDB = {"quiz 1" , "quiz 2" , "quiz3"};
    ArrayList<String> Quizzes;
    TextView tv;
    boolean SthSelected = true;

    public static String getQuiz_id() {
        return quiz_id;
    }

    private   static String quiz_id = "1";

    public static String getStudent_id() {
        return student_id;
    }

    private static String student_id = "1";

    int SpinPosition;
    JSONArray user = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_quiz);

        Quizzes = new ArrayList<String>();
        Quizzes.add("none");

        //connecting server:
        new LoadingQuizzes().execute();

        /*for(int i=0;i<quizzesfromDB.length;i++){
            Quizzes.add(quizzesfromDB[i]);
        }*/
        tv = (TextView) findViewById(R.id.textView);
        spinner = (Spinner) findViewById(R.id.spinner1);


        //for logging out
        btLogout = (Button) findViewById(R.id.logOut);
        btLogout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                UserFunctions logout = new UserFunctions();
                logout.logoutUser(getApplicationContext());
                Intent login = new Intent(getApplicationContext(), Login.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                finish();
            }
        });

        //for choosing quiz
        bt = (Button) findViewById(R.id.button2);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SthSelected) {
                    quiz_id = Quizzes.get(spinner.getSelectedItemPosition()).substring(5);
                    Intent myIntent = new Intent(getApplicationContext(), StartQuiz.class);
                    startActivity(myIntent);
                    finish();
                }
                else if(Quizzes.size() == 1){
                    tv.setTextColor(Color.BLUE);
                    tv.setText("No new quizzes are available");
                }
                else{
                    tv.setTextColor(Color.RED);
                    tv.setText("Please choose a quiz first to start");
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        SpinPosition = spinner.getSelectedItemPosition();
        if(SpinPosition == 0)
            SthSelected = false;
        else
            SthSelected = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //SthSelected = false;
    }

    private class LoadingQuizzes extends AsyncTask<String , String , JSONObject> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(ChooseQuiz.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... parameters) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.getQuizzes(student_id);
            return json;
        }



        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);

            try {
                // Getting JSON Array
                user = json.getJSONArray("quizes");
                for(int i=0;i<user.length();i++){
                    JSONObject jRealObj = user.getJSONObject(i);

                    Quizzes.add("quiz "+jRealObj.getString("quiz_id"));
                }
                ArrayAdapter <String> adapter = new ArrayAdapter<String>(ChooseQuiz.this , android.R.layout.simple_spinner_item , Quizzes);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(ChooseQuiz.this);
                pDialog.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}