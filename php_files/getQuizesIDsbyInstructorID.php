<?php



// array for JSON response
$response = array();

// include db connect class
require_once 'db.php';

// connecting to db
$db = new DB_CONNECT();

// check for post data
if (isset($_GET["instructor_id"])) {
    $pid = $_GET['instructor_id'];

    // get a product from products table
    $result = mysql_query("SELECT * FROM quizes WHERE instructor_id = $pid");
    // check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["quizIDs"] = array();

    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $product = array();
        $product["quiz_id"] = $row["id"];            
        // push single product into final response array
        array_push($response["quizIDs"], $product);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "Not found";

    // echo no users JSON
    echo json_encode($response);
}
}
?>
