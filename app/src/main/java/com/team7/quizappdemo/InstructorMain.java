package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.team7.quizappdemo.library.UserFunctions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class InstructorMain extends Activity {
    public String instructor_id = "1";
  //  private ProgressDialog pDialog;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_QUIZID = "quiz_id";
    private static final String TAG_GROUPID = "group_id";

    JSONParser_old jsonParser = new JSONParser_old();
    public static String getBt_text() {
        return bt_text;
    }

    private static String bt_text;

    // url to create new product
    private static String url_create_quiz = "http://mohamedessam.juplo.com/quiz_app/createAQuiz.php";
    private static String url_create_group = "http://mohamedessam.juplo.com/quiz_app/createAGroup.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructor_main);


        Button start = (Button) findViewById(R.id.start); //create quiz
        Button creategroup = (Button) findViewById(R.id.creategroup); //create group
        Button showResults = (Button) findViewById(R.id.showresults);
        Button showStatistics = (Button) findViewById(R.id.showstatistics);

        creategroup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new CreateAGroup().execute();

            }
        });

        start.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new CreateAQuiz().execute();

            }
        });


        showResults.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent ToResults = new Intent(getApplicationContext(), ChooseQuizToShowResultsAndStatistics.class);
                bt_text = "Results";
                startActivity(ToResults);
                finish();
            }
        });

        showStatistics.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent ToStatistics = new Intent(getApplicationContext(), ChooseQuizToShowResultsAndStatistics.class);
                bt_text = "Statistics";
                startActivity(ToStatistics);
                finish();
            }
        });
        Button btLogout = (Button) findViewById(R.id.logout);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserFunctions logout = new UserFunctions();
                logout.logoutUser(getApplicationContext());
                Intent login = new Intent(getApplicationContext(), Login.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                finish();
            }
        });
    }

    class CreateAQuiz extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //pDialog = new ProgressDialog(InstructorMain.this);
            //pDialog.setMessage("Creating Quiz..");
            //pDialog.setIndeterminate(false);
            //pDialog.setCancelable(true);
            //pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("instructor_id", instructor_id));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_quiz,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);
                String quiz_id = json.getString(TAG_QUIZID);

                if (success == 1) {
                    // successfully created product
                    Intent nxt = new Intent(getApplicationContext(),CreateQuizActivity.class);
                    nxt.putExtra("quiz_id",quiz_id);
                    nxt.putExtra("instructor_id",instructor_id);
                    startActivity(nxt);
                    finish();
                } else {
                    // failed to create product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            //pDialog.dismiss();
        }

    }
    class CreateAGroup extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //pDialog = new ProgressDialog(InstructorMain.this);
            //pDialog.setMessage("Creating Quiz..");
            //pDialog.setIndeterminate(false);
            //pDialog.setCancelable(true);
            //pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("instructor_id", instructor_id));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_group,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);
                String group_id = json.getString(TAG_GROUPID);

                if (success == 1) {
                    Intent nxt = new Intent(getApplicationContext(),CreateGroup.class);
                    nxt.putExtra("group_id",group_id);

                    startActivity(nxt);
                    finish();
                    // successfully created product

                } else {
                    // failed to create product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            //pDialog.dismiss();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
