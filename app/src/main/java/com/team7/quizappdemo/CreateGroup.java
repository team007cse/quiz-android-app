package com.team7.quizappdemo;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CreateGroup extends ListActivity {

    // Progress Dialog
    //private ProgressDialog pDialog;
    private ProgressDialog pDialog;
    // Creating JSON Parser object
    JSONParser_old jParser = new JSONParser_old();
    CheckBox checkBox;
    String pid;
    String group_id ;
    ArrayList<HashMap<String, String>> studentslist;

    // url to get all products list
    private static String url = "http://mohamedessam.juplo.com/quiz_app/getAllStudents.php";
    private static String url_post_a_student_to_group = "http://mohamedessam.juplo.com/quiz_app/PostAStudentToAgroup.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_STUDENTS = "students";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "name";
    public String q_id ; //Quiz ID
    public String instructor_id ; //Instructor ID

    // products JSONArray
    JSONArray students = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        //q_id = getIntent().getExtras().getString("quiz_id");
        //instructor_id = getIntent().getExtras().getString("instructor_id");
        // Hashmap for ListView
        group_id = getIntent().getExtras().getString("group_id");

        studentslist = new ArrayList<HashMap<String, String>>();

        // Loading products in Background Thread
        new LoadAllStudents().execute();
        ListView lv = getListView();

        // on seleting single group
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selectestItem
                pid = ((TextView) view.findViewById(R.id.student_id)).getText()
                        .toString();
                checkBox = (CheckBox) findViewById(R.id.check);
                // if (checkBox.isChecked()) {
                new PostAStudent().execute();
                //    }
            }
        });



    }


    // Response from Edit Product Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result code 100
        if (resultCode == 100) {
            // if result code 100 is received
            // means user edited/deleted product
            // reload this screen again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllStudents extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CreateGroup.this);
            pDialog.setMessage("Getting the Students..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * getting All Groups from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All Students: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    students = json.getJSONArray(TAG_STUDENTS);

                    // looping through All Products
                    for (int i = 0; i < students.length(); i++) {
                        JSONObject c = students.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);

                        // adding HashList to ArrayList
                        studentslist.add(map);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all groups
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */

                    ListAdapter adapter = new SimpleAdapter(
                            CreateGroup.this, studentslist,
                            R.layout.group_list, new String[] { TAG_PID,
                            TAG_NAME},
                            new int[] { R.id.student_id, R.id.check });
                    // updating listview
                    setListAdapter(adapter);
                }
            });

        }

    }
    class PostAStudent extends AsyncTask<String , String , String>{

        @Override
        protected String doInBackground(String... arg) {
            //for (int i = 0 ; i < 5 ; i++)
            //{

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("group_id", group_id));
            params.add(new BasicNameValuePair("student_id", pid));


            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jParser.makeHttpRequest(url_post_a_student_to_group,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product
                    //Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                    //startActivity(i);

                    // closing this screen
                    //finish();
                } else {
                    // failed to create product

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ProgressDialog pDialog = new ProgressDialog(CreateGroup.this);
            pDialog.setMessage("Choosing the student..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

        }
    }
}