package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.team7.quizappdemo.library.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ChooseQuizToShowResultsAndStatistics extends Activity implements AdapterView.OnItemSelectedListener{
    Spinner spinner;
    TextView tv;
    ArrayList<String> Quizzes;
    boolean sthSelected = true;
    String instructor_id = "1";
    String bt_text = InstructorMain.getBt_text();

    public static String getQuiz_size() {
        return quiz_size;
    }

    private static String quiz_size = "5";   //remove that initialization

    public static String getQuiz_id() {
        return quiz_id;
    }

    private static String quiz_id;
    JSONArray user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_quiz_to_show_results_and_statistics);

        bt_text = InstructorMain.getBt_text();
        Quizzes = new ArrayList<String>();
        Quizzes.add("none");

        //connecting server:
        new LoadQuizzes().execute();

        spinner = (Spinner) findViewById(R.id.quizes4instructor);
        tv = (TextView) findViewById(R.id.msg);

        Button bt1 = (Button) findViewById(R.id.show);
        bt1.setText("Show "+bt_text);

        bt1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(sthSelected) {
                    quiz_id = spinner.getSelectedItem().toString().substring(5);
                    new QuizSize().execute();

                    if(bt_text.equals("Results")){
                        Intent ToResults = new Intent(getApplicationContext(), ShowResults.class);
                        startActivity(ToResults);
                        finish();
                    }
                    else if(bt_text.equals("Statistics")){
                        Intent ToStatistics = new Intent(getApplicationContext(), ShowStatistics.class);
                        startActivity(ToStatistics);
                        finish();
                    }
                }
                else if(Quizzes.size() == 1){
                    tv.setTextColor(Color.BLUE);
                    tv.setText("No quizzes are made yet");
                }
                else{
                    tv.setTextColor(Color.RED);
                    tv.setText("Please choose a quiz first");
                }
            }
        });

    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int SpinPosition = spinner.getSelectedItemPosition();
        if(SpinPosition == 0)
            sthSelected = false;
        else
            sthSelected = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class LoadQuizzes extends AsyncTask<String , String , JSONObject> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(ChooseQuizToShowResultsAndStatistics.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... parameters) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.getQuizzesForInstructors(instructor_id);
            return json;
        }



        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);

            try {
                // Getting JSON Array
                user = json.getJSONArray("quizIDs");
                for(int i=0;i<user.length();i++){
                    JSONObject jRealObj = user.getJSONObject(i);

                    Quizzes.add("quiz "+jRealObj.getString("quiz_id"));
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChooseQuizToShowResultsAndStatistics.this , android.R.layout.simple_spinner_item , Quizzes);
                spinner.setAdapter(adapter);
                pDialog.dismiss();
                spinner.setOnItemSelectedListener(ChooseQuizToShowResultsAndStatistics.this);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class QuizSize extends AsyncTask<String , String , JSONObject> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(ChooseQuizToShowResultsAndStatistics.this);
            pDialog.setMessage("Submitting ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... parameters) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.getQuizSizes(quiz_id);
            return json;
        }



        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);

            try {
                // Getting JSON Array
                user = json.getJSONArray("numOfQ");
                JSONObject jRealObj = user.getJSONObject(0);

                quiz_size = jRealObj.toString();
                pDialog.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
