<?php



// array for JSON response
$response = array();

// include db connect class
require_once 'db.php';

// connecting to db
$db = new DB_CONNECT();

// check for post data
if (isset($_GET["quiz_id"])) {
    $pid = $_GET['quiz_id'];

    // get a product from products table
    $result = mysql_query("SELECT * FROM questions WHERE quiz_id = $pid");
    // check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["quiz"] = array();

    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $product = array();
        $product["id"] = $row["id"];
            $product["quiz_id"] = $row["quiz_id"];
            $product["question_header"] = $row["question_header"];
            $product["choose_a"] = $row["choose_a"];
            $product["choose_b"] = $row["choose_b"];
            $product["choose_c"] = $row["choose_c"];
            $product["choose_d"] = $row["choose_d"];
            $product["answer"] = $row["answer"];

        // push single product into final response array
        array_push($response["quiz"], $product);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "Not found";

    // echo no users JSON
    echo json_encode($response);
}
}
?>