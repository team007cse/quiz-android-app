package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.team7.quizappdemo.library.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;



public class ShowStatistics extends Activity {
    TextView tv;
    String quiz_id;
    ArrayList<Integer> resultsList;
    String[] available_results;
    int quiz_size;

    private View mChart;

    private static final String TAG_RESULT = "resultofquiz";
    private static final String KEY_SUCCESS = "success";


    JSONArray user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_statistics);

        tv = (TextView) findViewById(R.id.notSolved);
        quiz_id = ChooseQuizToShowResultsAndStatistics.getQuiz_id();
        resultsList = new ArrayList<Integer>();
        quiz_size = Integer.parseInt(ChooseQuizToShowResultsAndStatistics.getQuiz_size());
        available_results = new String[quiz_size+1];

        new LoadingResults().execute();

        Button back = (Button) findViewById(R.id.toMainstat);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),InstructorMain.class);
                startActivity(in);
                finish();
            }
        });
    }

    private double[] statistics (ArrayList<Integer> resList){
        double[] stat = new double[quiz_size+1];

        int order_to_increment;
        for(int i=0;i<=quiz_size;i++){
            stat[i] = 0;
            available_results[i] = Integer.toString(i);
        }
        for(int i=0;i<resList.size();i++){
            order_to_increment = resList.get(i);
            stat[order_to_increment] = stat[order_to_increment] +1;
        }
        return  stat;
    }


    private void openChart() {

        Integer[] results_in_array = (Integer[]) resultsList.toArray(new Integer[resultsList.size()]);
        double[] statistics_in_array = statistics(resultsList);
        int[] colors = { Color.BLUE, Color.MAGENTA, Color.GREEN, Color.RED ,Color.DKGRAY ,Color.YELLOW , Color.BLACK  , Color.GRAY};

        CategorySeries distributionSeries = new CategorySeries("Statistics about students' results");
        for (int i = 0; i <=quiz_size; i++) {
            distributionSeries.add(available_results[i], statistics_in_array[i]);
        }

        // Instantiating a renderer for the Pie Chart
        DefaultRenderer defaultRenderer = new DefaultRenderer();
        for (int i = 0; i <= quiz_size; i++) {
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            /*Random rand = new Random();
            float r = rand.nextFloat() * 256;
            float g = rand.nextFloat() * 256;
            float b = rand.nextFloat() * 256;
            Color randColor = new C*/
            seriesRenderer.setColor(colors[i%8]);
            seriesRenderer.setDisplayChartValues(true);
            //Adding colors to the chart
            defaultRenderer.setBackgroundColor(Color.CYAN);
            defaultRenderer.setApplyBackgroundColor(true);
            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer);
        }
        defaultRenderer.setChartTitle("Statistics about students' results");
        defaultRenderer.setChartTitleTextSize(35);
        defaultRenderer.setZoomButtonsVisible(false);


        LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart);
        // remove any views before u paint the chart
        chartContainer.removeAllViews();
        // drawing pie chart
        mChart = ChartFactory.getPieChartView(getBaseContext(),distributionSeries, defaultRenderer);
        // adding the view to the linearlayout
        chartContainer.addView(mChart);
    }


    private class LoadingResults extends AsyncTask<String , String , JSONObject> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(ShowStatistics.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... parameters) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.getSortedResults(quiz_id);
            return json;
        }



        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);

            try {
                if(json.getInt(KEY_SUCCESS) == 0){
                    pDialog.dismiss();
                    tv.setText("No students solved this quiz yet");
                }

                else{
                    // Getting JSON Array
                    user = json.getJSONArray("results");
                    for(int i=0;i<user.length();i++){
                        JSONObject jRealObj = user.getJSONObject(i);

                        resultsList.add(Integer.parseInt(jRealObj.getString(TAG_RESULT)));
                    }
                    pDialog.dismiss();
                    openChart();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
