package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import  com.team7.quizappdemo.library.DatabaseHandler;
import com.team7.quizappdemo.library.UserFunctions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StartQuiz extends Activity {
    TextView question_tv;
    RadioGroup choices;
    RadioButton r1;
    RadioButton r2;
    RadioButton r3;
    RadioButton r4;
    RadioButton SelectedOne;
    Button bt;
    ArrayList<ArrayList<String>> QAs;

    public static int getScore() {
        return score;
    }

    private static int score ;

    public static String getResult() {
        return result;
    }

    private static  String result;
    int order ;
    JSONArray user = null;
    String quizID ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_quiz);

        quizID = ChooseQuiz.getQuiz_id();

        QAs = new ArrayList<ArrayList<String>>();

        //connecting server:
        new LoadingQuestions().execute();

        //examination logic
        score = 0;
        order = 0;
        question_tv = (TextView) findViewById(R.id.textView1);
        choices = (RadioGroup) findViewById(R.id.radioGroup1);
        r1 = (RadioButton) findViewById(R.id.radioButton);
        r2 = (RadioButton) findViewById(R.id.radioButton2);
        r3 = (RadioButton) findViewById(R.id.radioButton3);
        r4 = (RadioButton) findViewById(R.id.radioButton4);
        bt = (Button) findViewById(R.id.button);

        bt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (order+2 >= QAs.size()){
                    bt.setText("Submit");
                }
                SelectedOne = (RadioButton) findViewById(choices.getCheckedRadioButtonId());
                if(QAs.get(order).get(5).equals(SelectedOne.getText().toString())){
                    score++;
                }
                if(order+1 >= QAs.size()){
                    result = "<b>" + "<i>" + DetermineGrade(score,QAs.size()) + "</i>" + "</b>" + "<br/>" + "Your score is "+ Integer.toString(StartQuiz.score) + "/" + Integer.toString(QAs.size());
                    Intent myIntent = new Intent(getApplicationContext(),ShowResult.class);
                    startActivity(myIntent);
                    finish();
                }

                order++;

                //bt.setText(Integer.toString(order));
                if(order < QAs.size()) {
                    question_tv.setText("Q" + Integer.toString(order + 1) + ": " + QAs.get(order).get(0));
                    r1.setText(QAs.get(order).get(1));
                    r2.setText(QAs.get(order).get(2));
                    r3.setText(QAs.get(order).get(3));
                    r4.setText(QAs.get(order).get(4));
                }
            }
        });


    }

    private String DetermineGrade (int StudentMark , int TotalMark){
        double ratio = (StudentMark * 1.0) / TotalMark ;
        if(ratio >= 0.85)
            return "Excellent";
        else if (ratio >= 0.75)
            return "Very Good";
        else if (ratio >= 0.65)
            return  "Good";
        else if (ratio >= 0.5)
            return  "Passed";
        else
            return "Sorry ! You failed";
    }

    private class LoadingQuestions extends AsyncTask <String , String , JSONObject>{

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(StartQuiz.this);
            pDialog.setMessage("Loading Questions ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... parameters) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.getQuestions(quizID);
            return json;
        }



        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);

            try {
                // Getting JSON Array
                user = json.getJSONArray("quiz");
                for(int i=0;i<user.length();i++){
                    JSONObject jRealObj = user.getJSONObject(i);

                    QAs.add(new ArrayList<String>(Arrays.asList( jRealObj.getString("question_header") , jRealObj.getString("choose_a") , jRealObj.getString("choose_b") , jRealObj.getString("choose_c") , jRealObj.getString("choose_d") , jRealObj.getString("answer"))));

                }

                if(QAs.size() == 1){
                    bt.setText("Submit");
                }


                //start adding questions
                question_tv.setText("Q" + Integer.toString(order + 1) + ": " + QAs.get(order).get(0));
                r1.setText(QAs.get(order).get(1));
                r2.setText(QAs.get(order).get(2));
                r3.setText(QAs.get(order).get(3));
                r4.setText(QAs.get(order).get(4));

                pDialog.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
