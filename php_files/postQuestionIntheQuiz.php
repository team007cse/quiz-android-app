<?php


// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['question_header']) && isset($_POST['choose_a']) && isset($_POST['choose_b'])&& isset($_POST['choose_c'])&& isset($_POST['choose_d'])&& isset($_POST['answer'])&& isset($_POST['answer'])&& isset($_POST['quiz_id'])) {

    $question_header = $_POST['question_header'];
    $choose_a = $_POST['choose_a'];
    $choose_b = $_POST['choose_b'];
    $choose_c = $_POST['choose_c'];
    $choose_d = $_POST['choose_d'];
    $answer = $_POST['answer'];
    $quiz_id = $_POST['quiz_id'];

    // include db connect class
    require_once 'db.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysql_query("INSERT INTO questions (question_header, choose_a, choose_b , choose_c,choose_d,answer,quiz_id) VALUES('$question_header', '$choose_a', '$choose_b', '$choose_c', '$choose_d', '$answer', '$quiz_id')");

    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Question successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";

        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>