package com.team7.quizappdemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class ChooseGroup extends ListActivity {

    // Progress Dialog
    //private ProgressDialog pDialog;
    private ProgressDialog pDialog;
    // Creating JSON Parser object
    JSONParser_old jParser = new JSONParser_old();

    ArrayList<HashMap<String, String>> groupsList;

    // url to get all products list
    private static String url = "http://mohamedessam.juplo.com/quiz_app/getAllGroups.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_GROUPS = "groups";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "name";
    public String q_id ; //Quiz ID
    public String instructor_id ; //Instructor ID

    // products JSONArray
    JSONArray groups = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_group);
        q_id = getIntent().getExtras().getString("quiz_id");
        instructor_id = getIntent().getExtras().getString("instructor_id");
        // Hashmap for ListView
        groupsList = new ArrayList<HashMap<String, String>>();

        // Loading products in Background Thread
        new LoadAllGroups().execute();

        // Get listview
        ListView lv = getListView();

        // on seleting single group
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selectestItem
                String pid = ((TextView) view.findViewById(R.id.group_id)).getText()
                        .toString();

                // Starting new intent
                Intent in = new Intent(getApplicationContext(),
                        QuizSending.class);
                // sending pid to next activity
                in.putExtra("group_id",pid);
                in.putExtra("quiz_id",q_id);
                in.putExtra("instructor_id",instructor_id);

                // starting new activity and expecting some response back
                startActivityForResult(in, 100);
            }
        });

    }

    // Response from Edit Product Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result code 100
        if (resultCode == 100) {
            // if result code 100 is received
            // means user edited/deleted product
            // reload this screen again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllGroups extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ChooseGroup.this);
            pDialog.setMessage("Getting the Groups..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * getting All Groups from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All Groups: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    groups = json.getJSONArray(TAG_GROUPS);

                    // looping through All Products
                    for (int i = 0; i < groups.length(); i++) {
                        JSONObject c = groups.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);

                        // adding HashList to ArrayList
                        groupsList.add(map);
                    }
                } else {
                    // no products found
                    // Launch Add New product Activity
                    Intent i = new Intent(getApplicationContext(),
                            InstructorMain.class);
                    // Closing all previous activities
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all groups
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */

                     ListAdapter adapter = new SimpleAdapter(
                            ChooseGroup.this, groupsList,
                            R.layout.list_group, new String[] { TAG_PID,
                            TAG_NAME},
                            new int[] { R.id.group_id, R.id.group_name });
                    // updating listview
                    setListAdapter(adapter);
                }
            });

        }

    }
}
