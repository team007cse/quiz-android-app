<?php



// array for JSON response
$response = array();

// include db connect class
require_once 'db.php';

// connecting to db
$db = new DB_CONNECT();

// check for post data
if (isset($_GET["quiz_id"])) {
    $pid = $_GET['quiz_id'];

    // get a product from products table
    $result = mysql_query("SELECT * FROM results WHERE quiz_id = $pid");
    // check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["results"] = array();

    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $product = array();
        $student_id = $row["student_id"];
        $result2 = mysql_query("SELECT * FROM students WHERE id = $student_id");
        while ($row2 = mysql_fetch_array($result2)) {
            $product['student_name'] = $row2['name'];
        }
        $product['resultofquiz'] = $row['resultofquiz'];
        // push single product into final response array
        array_push($response["results"], $product);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "Not found";

    // echo no users JSON
    echo json_encode($response);
}
}
?>
