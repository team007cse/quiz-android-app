package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class QuizSending extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    JSONParser_old jsonParser = new JSONParser_old();
    String quiz_id , group_id , instructor_id ;

    private static String url_of_sending_quiz = "http://mohamedessam.juplo.com/quiz_app/PostatQuizSending.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_sending);

         quiz_id = getIntent().getExtras().getString("quiz_id");
         group_id = getIntent().getExtras().getString("group_id");
         instructor_id = getIntent().getExtras().getString("instructor_id");
        new QuizSendingProcessing().execute();

        Button back = (Button) findViewById(R.id.back_to_main);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),ChooseGroup.class);
                startActivity(i);
                finish();
            }
        });

    }

    class QuizSendingProcessing extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(QuizSending.this);
            pDialog.setMessage("Sending Quiz..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            //String quiz_id = getIntent().getExtras().getString("quiz_id").toString();
            //String group_id = getIntent().getExtras().getString("group_id").toString();
            //String instructor_id = getIntent().getExtras().getString("instructor_id").toString();
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("group_id", group_id));
            params.add(new BasicNameValuePair("instructor_id", instructor_id));
            params.add(new BasicNameValuePair("quiz_id", quiz_id));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_of_sending_quiz,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product
                    //Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                    //startActivity(i);

                    // closing this screen
                    //finish();
                } else {
                    // failed to create product
                    TextView error = (TextView) findViewById(R.id.textView8);
                    error.setText("Not Sent");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz_sending, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
