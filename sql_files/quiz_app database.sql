-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2015 at 08:26 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_bin NOT NULL,
  `register_date` timestamp(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `register_date`) VALUES
(1, 'Team7', '2015-02-03 17:38:55.000000'),
(2, 'Team8', '2015-02-03 17:39:12.000000');

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE IF NOT EXISTS `instructors` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_bin NOT NULL,
  `password` varchar(150) COLLATE utf8_bin NOT NULL,
  `register_date` timestamp(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `question_header` text COLLATE utf8_bin NOT NULL,
  `choose_a` text COLLATE utf8_bin NOT NULL,
  `choose_b` text COLLATE utf8_bin NOT NULL,
  `choose_c` text COLLATE utf8_bin NOT NULL,
  `choose_d` text COLLATE utf8_bin NOT NULL,
  `answer` text COLLATE utf8_bin NOT NULL,
  `quiz_id` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_header`, `choose_a`, `choose_b`, `choose_c`, `choose_d`, `answer`, `quiz_id`) VALUES
(1, 'What''s your Name', 'Mo', 'Lo', 'Qa', 'So', 'Mo', 1),
(2, 'Your Country', 'Eg', 'Us', 'As', 'La', 'Eg', 1),
(3, 'What''s your Name', 'Mo', 'Lo', 'Qa', 'So', 'Mo', 1),
(4, 'Your Country', 'Eg', 'Us', 'As', 'La', 'Eg', 1),
(5, 'Age?', '19', '20', '21', '22', '21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quizes`
--

CREATE TABLE IF NOT EXISTS `quizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instructor_id` int(15) NOT NULL,
  `group_id` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quizes`
--

INSERT INTO `quizes` (`id`, `instructor_id`, `group_id`) VALUES
(1, 1, 1),
(2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `quiz_id` int(15) NOT NULL,
  `resultofquiz` varchar(15) COLLATE utf8_bin NOT NULL,
  `ifExam` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `student_id`, `quiz_id`, `resultofquiz`, `ifExam`) VALUES
(1, 1, 1, '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_bin NOT NULL,
  `password` varchar(150) COLLATE utf8_bin NOT NULL,
  `group_id` int(15) NOT NULL,
  `register_date` timestamp(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `password`, `group_id`, `register_date`) VALUES
(1, 'Mohamed Essam', '', 1, '2015-02-02 15:44:51.000000'),
(2, 'Mohamed Talaat', '', 1, '2015-02-02 15:44:51.000000'),
(3, 'Mohamed LOL', '', 1, '2015-02-02 15:47:12.000000'),
(4, 'Steve Austin', '', 1, '2015-02-02 15:47:12.000000');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
