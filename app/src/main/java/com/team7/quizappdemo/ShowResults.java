package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.team7.quizappdemo.library.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


public class ShowResults extends Activity {
    String quiz_id;
    int quiz_size;
    ArrayList<HashMap<String, String>> studentsList , titleList;

    ListAdapter adapter_title, adapter;
    TextView tv;

    private static final String TAG_ORDER = "order";
    private static final String TAG_NAME = "student_name";
    private static final String TAG_RESULT = "resultofquiz";
    private static final String KEY_SUCCESS = "success";



    JSONArray user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);

        //initialization
        quiz_id = ChooseQuizToShowResultsAndStatistics.getQuiz_id();
        studentsList = new ArrayList<HashMap<String, String>>();
        titleList = new ArrayList<HashMap<String, String>>();
        quiz_size = Integer.parseInt(ChooseQuizToShowResultsAndStatistics.getQuiz_size());

        new LoadingResults().execute();


        Button back = (Button) findViewById(R.id.toMainres);
        tv = (TextView) findViewById(R.id.tvtest);



        //tv.setText(Integer.toString(studentsList.size())+quiz_id+quiz_size);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),InstructorMain.class);
                startActivity(in);
                finish();
            }
        });


    }

    public void showList(){
        ListView list,list_head;
        list_head = (ListView) findViewById(R.id.listView1);
        list = (ListView) findViewById(R.id.listView2);
        HashMap<String,String> titles = new HashMap<String,String>();
        titles.put("order","Order");
        titles.put("StudentName","Student Name");
        titles.put("StudentScore","Student Score");
        titleList.add(titles);

        try {
            adapter_title = new SimpleAdapter(this, titleList, R.layout.row,
                    new String[] { "order", "StudentName", "StudentScore" }, new int[] {
                    R.id.order, R.id.name, R.id.score });
            list_head.setAdapter(adapter_title);
        } catch (Exception e) {

        }

        try {
            adapter = new SimpleAdapter(this, studentsList , R.layout.row,
                    new String[] { TAG_ORDER, TAG_NAME, TAG_RESULT }, new int[] {
                    R.id.order, R.id.name, R.id.score });
            list.setAdapter(adapter);
        } catch (Exception e) {

        }
    }



    private class LoadingResults extends AsyncTask<String , String , JSONObject> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShowResults.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... parameters) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.getSortedResults(quiz_id);
            return json;
        }



        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);

            try {
                if(json.getInt(KEY_SUCCESS) == 0){
                    pDialog.dismiss();
                    tv.setText("No students solved this quiz yet");
                }
                else{
                    // Getting JSON Array
                    user = json.getJSONArray("results");
                    for(int i=0;i<user.length();i++){
                        JSONObject jRealObj = user.getJSONObject(i);

                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ORDER , Integer.toString(i+1));
                        map.put(TAG_NAME, jRealObj.getString(TAG_NAME));
                        map.put(TAG_RESULT, jRealObj.getString(TAG_RESULT));

                        studentsList.add(map);
                    }
                    pDialog.dismiss();

                    //for sorting arrayList
                    Collections.sort(studentsList, new Comparator<Map<String, String>>() {

                        final static String COMPARE_KEY = TAG_RESULT;

                        @Override
                        public int compare(Map<String, String> lhs, Map<String, String> rhs) {
                            String v1 = lhs.get(COMPARE_KEY);
                            String v2 = rhs.get(COMPARE_KEY);
                            return v1.compareTo(v2);
                        }
                    });

                    //for reversing arrayList
                    ArrayList<HashMap<String,String>> tempElements = new ArrayList<HashMap<String,String>>(studentsList);
                    Collections.reverse(tempElements);
                    studentsList = tempElements;

                    //for rearranging order
                    for(int i=0;i<studentsList.size();i++){
                        studentsList.get(i).put(TAG_ORDER,Integer.toString(i+1));
                    }
                    showList();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
