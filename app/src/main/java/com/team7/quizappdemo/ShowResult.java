package com.team7.quizappdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import  com.team7.quizappdemo.library.DatabaseHandler;
import com.team7.quizappdemo.library.UserFunctions;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ShowResult extends Activity {
    TextView res;
    TextView ErrorMsg;
    Button tr;
    Button btnLogout;
    private ProgressDialog pDialog;
    JSONParser_old jsonParser = new JSONParser_old();
    private static final String TAG_SUCCESS = "success";


    private static String url_of_sending_result = "http://mohamedessam.juplo.com/quiz_app/PostResult.php";

    String student_id = ChooseQuiz.getStudent_id();
    String quiz_id = ChooseQuiz.getQuiz_id();
    String resultofquiz = Integer.toString(StartQuiz.getScore());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        res = (TextView) findViewById(R.id.textView8);
        tr = (Button) findViewById(R.id.button3);
        ErrorMsg = (TextView) findViewById(R.id.errorScore);

        new postingResult().execute();

        res.setText(Html.fromHtml(StartQuiz.getResult()));
        tr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getApplicationContext(),ChooseQuiz.class);
                startActivity(myIntent);
                finish();
            }
        });

        btnLogout = (Button) findViewById(R.id.loggingOut);
        btnLogout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                UserFunctions logout = new UserFunctions();
                logout.logoutUser(getApplicationContext());
                Intent login = new Intent(getApplicationContext(), Login.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                finish();
            }
        });

    }

    class postingResult extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShowResult.this);
            pDialog.setMessage("Sending Result..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("quiz_id", quiz_id));
            params.add(new BasicNameValuePair("student_id", student_id));
            params.add(new BasicNameValuePair("resultofquiz", resultofquiz));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_of_sending_result,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product
                    //Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                    //startActivity(i);

                    // closing this screen
                    //finish();
                } else {
                    // failed to create product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }


}